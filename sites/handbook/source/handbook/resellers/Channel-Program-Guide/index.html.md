---
layout: handbook-page-toc
title: "GitLab Channel Program Guide"
description: "GitLab Channel partner program guide."
---
## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

**We invite organizations interested in becoming a GitLab Channel Partner to [APPLY NOW](https://partners.gitlab.com/English/register_email.aspx).**

## GitLab Channel Partner Program & Guide


### How the Program Works

Partner Program Tracks &  Designations

The GitLab Partner Program consists of two tracks to support the different ways our partners go to market:



* Open – This track is for all partners (resellers, integrators, sales, and services) in the DevOps space , IT as a service (ITaaS) and other adjacent spaces that are committed to investing in their DevOps practice buildout. GitLab Open partners may or may not be transacting partners, and qualifying partners can earn product discounts, referral fees and services incentives.
* Select – In this track, partners make a greater investment in GitLab sales expertise develop services practices around GitLab, and are expected to drive greater GitLab product recurring revenues. In return, Select partners have a dedicated Channel Account Manager (CAM) and greater investment. The Select track is by invitation only.
* Professional Services Partner (PSP) - this partner designation highlights your team’s capabilities in helping customers get up and running with GitLab as well as maximizing those customers’ ROI in GitLab.


### GitLab Partner Program, Value for Partners

GitLab is experiencing tremendous growth, which translates to incredible opportunities for our partners.


<table>
  <tr>
   <td>GitLab Value
   </td>
   <td>What's included
   </td>
  </tr>
  <tr>
   <td>GitLab Sales & Referrals
   </td>
   <td>
<ul>

<li>Product sales discounts

<li>Renewals discounts

<li>Referral fees

<li>Partner services and other incentives

<li>Recurring revenue
</li>
</ul>
   </td>
  </tr>
  <tr>
   <td>Customer Adoption Services Opportunity
   </td>
   <td>
<ul>

<li>Enhance your consulting practice

<li>Deployment services including: implementation, integration, migration

<li>Adoption services including: education, enablement, and process optimization

<li>Build Managed Services offerings leveraging GitLab
</li>
</ul>
   </td>
  </tr>
  <tr>
   <td>Most Complete DevOps Platform in the market
   </td>
   <td>
<ul>

<li>The entire DevOps lifecycle in one application

<li>New features released every month

<li>Transparent product development where you can contribute and collaborate both directly and on behalf of customers
</li>
</ul>
   </td>
  </tr>
  <tr>
   <td>Ecosystem of Technology and Cloud Partners
   </td>
   <td>
<ul>

<li>Multi cloud platform support

<li>Build off your existing partner relationships

<li>Integration services opportunities
</li>
</ul>
   </td>
  </tr>
  <tr>
   <td>Investment in Partner Success
   </td>
   <td>
<ul>

<li>Portal for enablement resources and deal management

<li>Sales training and resources

<li>Services training and service framework guidance

<li>Customer adoption guidance

<li>Webinars focusing on sales, services, and adoption success

<li>Not-for-Resale (NFR) licenses, for your labs and demos
</li>
</ul>
   </td>
  </tr>
</table>



### Program Benefits & Requirements


<table>
  <tr>
   <td>
   </td>
   <td>GitLab Open
   </td>
   <td>GitLab Select
<p>
(Invitation only)
   </td>
  </tr>
  <tr>
   <td>Overall Program Requirements
   </td>
   <td>
   </td>
   <td>
   </td>
  </tr>
  <tr>
   <td>GitLab Partner Agreement
   </td>
   <td>X
   </td>
   <td>X
   </td>
  </tr>
  <tr>
   <td># of Accredited Sales Professional or existing Verified Sales Associate (Sales Core) resources
   </td>
   <td>2
   </td>
   <td>4
   </td>
  </tr>
  <tr>
   <td># of Solutions Architect Verified Associates 
   </td>
   <td>1
   </td>
   <td>2
   </td>
  </tr>
  <tr>
   <td># of GitLab Partner Technical Engineers (PTE) or existing Professional Services (PSE) Certified resources
   </td>
   <td>
   </td>
   <td>1
   </td>
  </tr>
  <tr>
   <td>Exec Sponsored Joint Business Plan
   </td>
   <td>
   </td>
   <td>X
   </td>
  </tr>
  <tr>
   <td>Program Rev Targets Min - BusPlan Committed
   </td>
   <td>
   </td>
   <td>$300K
   </td>
  </tr>
  <tr>
   <td>
   </td>
   <td>
   </td>
   <td>
   </td>
  </tr>
  <tr>
   <td>Sales Benefits
   </td>
   <td>
   </td>
   <td>
   </td>
  </tr>
  <tr>
   <td>Deal Registration Discounts
   </td>
   <td>X
   </td>
   <td>X
   </td>
  </tr>
  <tr>
   <td>Referral Fee
   </td>
   <td>X
   </td>
   <td>X
   </td>
  </tr>
  <tr>
   <td>Renewals Discounts
   </td>
   <td>X
   </td>
   <td>X
   </td>
  </tr>
  <tr>
   <td>Discounts for Reselling GitLab Professional Services
   </td>
   <td>X
   </td>
   <td>X
   </td>
  </tr>
  <tr>
   <td>
   </td>
   <td>
   </td>
   <td>
   </td>
  </tr>
  <tr>
   <td>Support Benefits
   </td>
   <td>
   </td>
   <td>
   </td>
  </tr>
  <tr>
   <td>Partner Helpdesk
   </td>
   <td>X
   </td>
   <td>X
   </td>
  </tr>
  <tr>
   <td>Level 2 Support Hotline
   </td>
   <td>
   </td>
   <td>X
   </td>
  </tr>
  <tr>
   <td>Dedicated Channel Manager
   </td>
   <td>
   </td>
   <td>X
   </td>
  </tr>
  <tr>
   <td>Not for Resale Licenses
   </td>
   <td>X
   </td>
   <td>X
   </td>
  </tr>
  <tr>
   <td>
   </td>
   <td>
   </td>
   <td>
   </td>
  </tr>
  <tr>
   <td>Marketing
   </td>
   <td>
   </td>
   <td>
   </td>
  </tr>
  <tr>
   <td>Partner Portal
   </td>
   <td>X
   </td>
   <td>X
   </td>
  </tr>
  <tr>
   <td>Proposal Based MDF
   </td>
   <td>Through Distribution
   </td>
   <td>X
   </td>
  </tr>
  <tr>
   <td>Sales & Pre-Sales Technical Enablement
   </td>
   <td>X
   </td>
   <td>X
   </td>
  </tr>
  <tr>
   <td>Program Logo
   </td>
   <td>X
   </td>
   <td>X
   </td>
  </tr>
  <tr>
   <td>Program & Certification Badges
   </td>
   <td>X
   </td>
   <td>X
   </td>
  </tr>
  <tr>
   <td>GitLab.com Listing
   </td>
   <td>X
   </td>
   <td>X (featured)
   </td>
  </tr>
  <tr>
   <td>Demand Generation Resources
   </td>
   <td>X
   </td>
   <td>X
   </td>
  </tr>
  <tr>
   <td>Leads
   </td>
   <td>
   </td>
   <td>X
   </td>
  </tr>
  <tr>
   <td>
   </td>
   <td>
   </td>
   <td>
   </td>
  </tr>
  <tr>
   <td>Services
   </td>
   <td>
   </td>
   <td>
   </td>
  </tr>
  <tr>
   <td>Rebates for Attaching Your Services to GitLab Sales
   </td>
   <td>X (for 

<p id="gdcalert1" ><span style="color: red; font-weight: bold">>>>>>  gd2md-html alert: undefined internal link (link text: "PSPs"). Did you generate a TOC with blue links? </span><br>(<a href="#">Back to top</a>)(<a href="#gdcalert2">Next alert</a>)<br><span style="color: red; font-weight: bold">>>>>> </span></p>

<a href="#heading=h.2yylnwkw673s">PSPs</a>)
   </td>
   <td>X
   </td>
  </tr>
  <tr>
   <td>Professional Services Designations, Training, Accreditations, & Certifications
   </td>
   <td>X
   </td>
   <td>X
   </td>
  </tr>
  <tr>
   <td>
   </td>
   <td>
   </td>
   <td>
   </td>
  </tr>
  <tr>
   <td>Managed Services
   </td>
   <td>
   </td>
   <td>
   </td>
  </tr>
  <tr>
   <td>Managed Services Discount
   </td>
   <td>X
   </td>
   <td>X
   </td>
  </tr>
</table>



### GitLab Channel Services Program

The GitLab Channel Services program is designed to help new or existing partners design service portfolios around the DevOps Lifecycle in correlation to GitLab products. The program will help partners evaluate business opportunities and create a technical enablement framework to use as they build their GitLab-related service practices.

The foundation of the GitLab Channel Services Program consists of two elements on which partners can build upon to start or grow their GitLab-related service portfolio:



1. [Designations](https://about.gitlab.com/handbook/resellers/services/#gitlab-certified-service-partner-program-overview): GitLab offers GitLab Service Partner Designations, that include training for service delivery teams which enable partners to meet program compliance requirements.
2. [GitLab Channel Service Packages](https://partners.gitlab.com/prm/English/c/Services): Partners can utilize GitLab Channel Service Packages to assist their teams as they build the service offerings, market and sell their services, and support their service business growth.


#### Becoming a Service Partner

All GitLab Service Partner Designations have program compliance and training requirements that must be completed before you can obtain the associated badge.

The GitLab Channel team will communicate the award in email, and reflect the certification in the GitLab Partner Locator.

All GitLab Service Partner Badges are reviewed periodically. Non-compliant partners at the time of the review will have one quarter to return to compliance.


#### GitLab Professional Services Partner Requirements



<p id="gdcalert2" ><span style="color: red; font-weight: bold">>>>>>  gd2md-html alert: inline image link here (to images/image1.png). Store image on your image server and adjust path/filename/extension if necessary. </span><br>(<a href="#">Back to top</a>)(<a href="#gdcalert3">Next alert</a>)<br><span style="color: red; font-weight: bold">>>>>> </span></p>


![alt_text](images/image1.png "image_tooltip")



<table>
  <tr>
   <td>Program Entry Requirements
   </td>
   <td>Each PSP must be:
<ol>

<li>Be a Open or Select GitLab Partner or GitLab Distribution Partner

<li>Design, build and operate a professional service practice, and

<li>Hire team members who have completed the competency requirements and/or sponsor the appropriate number of team members through completion of the competency requirements
</li>
</ol>
   </td>
  </tr>
  <tr>
   <td>Competency Requirements
   </td>
   <td>Each PSP must remain compliant with the either the Open, Select, or Distribution programs, and perpetually employ at least three (3) <a href="https://about.gitlab.com/handbook/resellers/training/#gitlab-professional-services-engineer-pse">GitLab Certified Professional Service Engineers</a>
<ol>

<li>Partner organizations who achieved their PSP prior to Nov. 4, 2021 are required to have three (3) <a href="https://about.gitlab.com/handbook/resellers/training/#gitlab-professional-services-engineer-pse">GitLab Certified Professional Service Engineers</a> on staff prior to November 1, 2022
</li>
</ol>
   </td>
  </tr>
  <tr>
   <td>Service Offerings
   </td>
   <td>Any or all of the following services can be provided by an PSP:
<p>
* Assessment
<p>
* Migration
<p>
* Integration
<p>
* Implementation
<p>
* Optimization
<p>
* Security/Compliance
   </td>
  </tr>
  <tr>
   <td>Compliance Requirements
   </td>
   <td>Each PSP must:
<p>
* Hire and continually employ team members who achieve and maintain the competency requirements
<p>
* Maintain positive Customer Success ratings measured against the following <a href="https://about.gitlab.com/handbook/customer-success/vision/#time-to-value-kpis">Time-to-Value KPIs</a> that GitLab uses for its metrics: Infrastructure Ready, First Value, & Outcome Achieved.
   </td>
  </tr>
</table>



#### Maintaining Service Partner Badges

At GitLab, collaboration and feedback is meant to nurture and mentor our service partners to grow their service capabilities and expertise. The program will utilize our internal [customer success metrics](https://about.gitlab.com/handbook/customer-success/vision/#measurement-and-kpis) to measure and understand the GitLab customer experience across the entire ecosystem and help further develop our service partners. In order to work together in this process, we will ask each service partner to provide evidence (as stated in the program descriptions above) to support the periodic review for your customer set one month before it is due.

GitLab Channel Partner program will review the GitLab Service Partners’ customer success rating and practitioner certification status each year in the month the partner was originally granted certification. Partners will be notified with the outcome of the review and the resulting status of their badge. If the partner needs to make further investments to stay in good standing with regards to Service Partner badge, the email will indicate the existing status and state the expected status with a due date when compliance is required to renew your GitLab Service Partner Badge. Partners can work with their GitLab Channel Account Manager (CAM) to create a plan for obtaining and maintaining badges.


### GitLab Channel Partner Program Guide

The [GitLab Channel Partner Program Guide](https://about.gitlab.com/handbook/resellers/Channel-Program-Guide/) offers a complete overview of the program. 

### Contact Us


## All authorized GitLab resellers are invited to the GitLab #resellers Slack channel. This Slack channel allows you to reach out to our sales and marketing team in a timely manner, as well as other resellers.


## Additionally, you can email the GitLab Channel Team at partnersupport@gitlab.com
