---
layout: handbook-page-toc
title: "GitLab Assembly"
description: "An overview of GitLab Assembly and Quarterly Kickoff Meetings"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Overview

GitLab has two, big quarterly meetings to align team members. One is Assembly. The other is Quarterly Kickoff. This page provides an overview of each of these. 

## GitLab Assembly

GitLab Assembly takes place each quarter and is part of a series of events intended to share timely information with our team members. Assembly happens after reporting our earnings to share highlights of the past quarter and celebrate our successes, milestones, and results.

Timing of team member touchpoints including Assembly:

- [Quarterly Kickoff](https://about.gitlab.com/handbook/ceo/chief-of-staff-team/#quarterly-kickoff) (first half of the first month of the quarter): Typically a video update from GitLab CEO, Sid Sibrandij shared on Slack, followed by an AMA
- Earnings Call (first half of the second month of the quarter): Public call that team members are invited to attend
- Earnings Recap & AMA (immediately following earnings): A 25min private AMA occurring after Earnings
- Assembly (end of second month, early the third month of a quarter): An all-company meeting occurring 2-3 weeks after Earnings that covers a wide range of topics

### GitLab Assembly Schedule

Quarterly: Three standalone GitLab Assemblies per year, one GitLab Assembly-type content incorporated into [GitLab Contribute](https://about.gitlab.com/events/gitlab-contribute/){:data-ga-name="Gitlab contribute"}{:data-ga-location="body"}. For example, in FY22, the GitLab Assembly schedule was:

1. Q1 Assembly: [Fiscal year kickoff](https://about.gitlab.com/company/gitlab-assembly/#fy22-gitlab-assembly){:data-ga-name="Fiscal year kickoff"}{:data-ga-location="body"}
1. Q2 Assembly: Recap Q1 and look forward to rest of Q2
1. Q3 Assembly: Recap Q2 and look forward to rest of Q3
1. Q4 Assembly: Recap Q3 and look forward to year-end/Q4

### GitLab Assembly Format

Assembly is a quarterly, company-wide 40 minute meeting consisting of ~25 mins content and ~15 mins Q&A. It is scheduled at two separate times so team members in all time zones have the opportunity to attend.

The call concludes with an AMA with all of E-Group. Team members can ask questions of any particular executive. The AMA doc will be circulated early so that team members can submit questions.

[Team members should join muted](/handbook/tools-and-tips/zoom/setting-up-a-zoom-meeting#large-meeting-considerations), but they can unmute themselves as required to effectively participate in the conversation. 

### GitLab Assembly Content

GitLab Assembly content creation is led by the [People Comms & Engagement](https://handbook.gitlab.com/job-families/people-group/people-communications-engagement/){:data-ga-name="internal comms"}{:data-ga-location="body"} team and [E-Group](https://about.gitlab.com/company/team/structure/#executives){:data-ga-name="e-group"}{:data-ga-location="body"}.

This is a [People Comms & Engagement](https://handbook.gitlab.com/job-families/people-group/people-communications-engagement/){:data-ga-name="internal comms initiative"}{:data-ga-location="body"} initiative and @kaylagolden is the DRI. The meeting is scheduled by the [EBA to the CEO](https://handbook.gitlab.com/job-families/people-group/executive-business-administrator/){:data-ga-name="EBA"}{:data-ga-location="body"}. The call is recorded and posted to Google Drive. The recording is internal only because we share sensitive information confidential to the company at the meeting.

### GitLab Assembly Timeline for delivery

Starting in FY23-Q4, the following timeline will be used to prepare key deliverables that will be needed to hold the event in close proximity to our [earnings call](/handbook/finance/investor-relations/#quarterly-earnings-process). For a more detailed list of items needed for delivery, please visit the [People Communications & Engagement](/handbook/people-group/employment-branding/people-communications/#gitlab-assembly) handbook page.

1. 4 weeks after the prior quarters earnings call: set time/date for Assembly (note this isn't sent out to all team members until after the earnings call date has been announced)
1. 4 weeks prior to Assembly: have host & speakers identified
1. 4 weeks prior to Assembly: Assembly CEO content draft prepared, five review sessions scheduled with Sid spread over the next two weeks
1. 3 weeks prior to Assembly: Begin feedback sessions with Sid and continuously iterate on the feedback
1. 2 weeks prior to Assembly: Meeting invites sent to all team members, content "pencils down"
1. 2 weeks prior to Assembly: Kickoff Legal review of CEO content
1. 1 week prior to Assembly: CEO content finalized, and video recorded with Sid
1. 1 week prior to Assembly: Legal review of video after recording
1. 1 week prior to Assembly: Work with the digital production team to have the slides and video edited together. Get Legal review of this as well.

### GitLab Assembly Goals

1. 📈 Review values, mission, vision, strategy
1. 🤝 Review of the past quarter and celebration of our success, milestones, and results
1. 👁 Transparent AMA with a majority of the company in attendance
1. 🌐 Network with global team members

### GitLab Assembly Metrics

Attendance:

1. FY22-Q4: >65% global attendance
1. FY23-Q1: >70% global attendance
1. FY23-Q2: >60% global attendance
1. FY23-Q3: >60% global attendance

### FY22 GitLab Assembly

The FY22 GitLab Assembly (previously called Fiscal Year Kickoff) took place 2021-02-18 at 8:00am and 5:00pm Pacific Time. It was attended live by approximately 800 team members, with remaining team members encouraged to watch the recording asynchronously.

The event was internal because it featured financial metrics and other information which was not public. It was recorded, and the recording is available [in Drive](https://drive.google.com/file/d/1V_yohghvDpKQf4sXlNGe_6LgscVjxfXo/view?usp=sharing).

This event was approximately 40 minutes long and was hosted on Hopin. The agenda was as follows:

1. Welcome
1. FY21/22 Highlights - [CEO](https://handbook.gitlab.com/job-families/chief-executive-officer/){:data-ga-name="CEO"}{:data-ga-location="body"} & [CRO](https://handbook.gitlab.com/job-families/sales/chief-revenue-officer/){:data-ga-name="CRO"}{:data-ga-location="body"}
1. GitLab is in Rarefied Air - [CPO](https://handbook.gitlab.com/job-families/people-group/chief-people-officer/){:data-ga-name="CPO"}{:data-ga-location="body"} & [CFO](https://handbook.gitlab.com/job-families/finance/chief-financial-officer/){:data-ga-name="CFO"}{:data-ga-location="body"}
1. Marketing Vision - [CMO](https://handbook.gitlab.com/job-families/marketing/chief-marketing-officer/){:data-ga-name="CMO"}{:data-ga-location="body"}
1. Product Themes - [Chief Product Officer](https://handbook.gitlab.com/job-families/product/chief-product-officer/){:data-ga-name="chief product officer"}{:data-ga-location="body"}
1. Product Vision - [VP Product Management](https://handbook.gitlab.com/job-families/product/product-management-leadership/#vp-of-product-management){:data-ga-name="VP product management"}{:data-ga-location="body"}
1. Engineering Vision - [CTO](https://handbook.gitlab.com/job-families/engineering/engineering-management/#chief-technology-officer){:data-ga-name="CTO"}{:data-ga-location="body"}
1. FY22 Vision - CEO & CRO
1. Closing and randomized coffee chats

Based on [feedback from the FY21 kickoff](#improvements-to-be-made){:data-ga-name="FY21 feedback"}{:data-ga-location="body"} and in the spirit of iteration, there were some changes to the format. Changes included:

* Adding a produced video, created mostly using highlights from [Sales Kickoff](/handbook/sales/training/SKO/){:data-ga-name="SKO"}{:data-ga-location="body"} presentations, to share the most important information. This mostly replaced the previous format of sequential 5-minute live updates.
* Adding a second time to be inclusive of all time zones.
* During each event, attendees watched the video update live (no requirement to complete any actions before the meeting). The video is followed by an AMA with the executive team.
* More focus on the big picture, inspiration, and team alignment.
* Inclusivity: the video was captioned and industry terms, acronyms, or jargon which aren’t well-known were defined.

[Read the feedback](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/-/issues/4196){:data-ga-name="Read the feedback"}{:data-ga-location="body"} from GitLab Assembly FY22 Kickoff

## Quarterly Kickoff

The CEO has a Quarterly Kickoff in the first month of each quarter. 

This is a forum to review our long-term goals, recognize progress made in the previous quarter, recap significant recent events, and discuss what we aim to accomplish in the coming quarter. Material shared is internal only. 

The Quarterly Kickoff is followed by an AMA hosted by the CEO. 

The CEO's Quarterly Kickoff Slides usually cover:
- Recap of GitLab’s [Mission](/company/mission/)/[Vision](/company/vision/)/[Strategy](/company/strategy/) noting any updates made
- Reviewing which [Top Cross-Functional Initiatives](/company/top-cross-functional-initiatives/) were retired and added and why
- Review the previous quarters OKRs, scores, and important details
- Progress for [Yearlies](/company/yearlies/)
- Sharing of this new quarters OKRs
- Marathon Slide (as needed)
- Reiterate how all elements of [cadence fit together](/company/yearlies/) including any new or changed elements of hierarchy such as new OKRs. 

### Timeline for the Quarterly Kickoff

1. First week of the last month of the current quarter - pick the date in the first month of the next quarter for the kickoff. The preferred date to share the video is on the Monday of the second full week of the month. Work with internal comms to validate this date and schedule an AMA the Wednesday which follows with the appropriate invite list
1. Second week of the last month of the current quarter - begin [preparing the slides](#how-to-prepare-the-slides) and internal comms announcement
   1. [FY23-Q4 Quarterly Kickoff](https://docs.google.com/presentation/d/1ItrJIJWIwnDQ13KNtuBDFRTdSa6nLpAeBXa4vi4i4ws)
   1. Once done with an initial iteration, solicit feedback from the Chief of Staff to the CEO as well as any other colleagues who can help
   1. Include the timeline, logistics, and draft message for the internal comms announcement
       - [Example from the FY23-Q4 Quarterly Kickoff](https://docs.google.com/document/d/1a2C3h24wvYn4qYpNKNnweW-Wx1BQ4ZRX50StO7XmbkU)
1. Third week of the last month of the current quarter - schedule three review/recording sessions with the CEO. The first session is a review only and in the last week of the quarter. The two review/recording sessions should follow quarter close to ensure any needed information is included in the deck.
   1. Once the recording dates are more certain, file a [video editing request](https://gitlab.com/gitlab-com/marketing/inbound-marketing/global-content/digital-production/-/issues/new?issuable_template=video-editing-request) with the Digital Production team so they can prepare to merge the slides and the video together.
      - [Example issue from FY23-Q4 Quarterly Kickoff](https://gitlab.com/gitlab-com/marketing/inbound-marketing/global-content/digital-production/-/issues/339)
    1. Reach out to the Learning and Development team, specifically [Jamie Allen](https://gitlab.com/jallen16), to let them know this is coming to be posted on `Level Up`
    1. After the first review session and the majority of the feedback is addressed, use the [Single material legal review process](https://about.gitlab.com/handbook/legal/materials-legal-review-process/#track-1-single-material-legal-review-process) to get Legals review of the content and announcement
       - [Example issue from FY23-Q4 Quarterly Kickoff](https://gitlab.com/gitlab-com/legal-and-compliance/-/issues/1237)
    1. Use the following settings in Zoom to record. It is best for the recorder to own the Zoom link in the invite otherwise the recorder needs to be granted permission.
        1. Pin the CEO
        1. Change `View` to `Speaker`
        1. Enable `Hide Self View`
        1. Enable `Hide Non-video participants`
        1. Ask everyone except the CEO to mute their audio/video
        1. Click `Record` -> `Record on this Computer`
    1. After the recording has been made share the raw recording with Legal for quick feedback
    1. Review the recording and in the speaker notes of the slides add the appropriate timestamps for the Digital Production team to use to show the correct slide in the video
    1. Update the issue with the Digital Production team to link to the video and slides. Makes sure permissions are correct so they are only shared with the needed DRIs.
    1. Once the video is returned, review it for correctness. Once reviewed, ask for Legal's final sign off.
    1. Reach out to the Learning and Development team, specifically [Jamie Allen](https://gitlab.com/jallen16), to share the video with them so they can post it to `Level Up`. They will share a special URL which will authenticate folks via Okta if they are not already authenticated. Make sure to use this URL when sharing. For example, FY23-Q4's Quarterly Kickoff URL was: 
    [`https://levelup.gitlab.com/access/saml/login/internal-team-members?returnTo=https://levelup.gitlab.com/learn/video/fy23-q4-quarterly-kickoff`](https://levelup.gitlab.com/access/saml/login/internal-team-members?returnTo=https://levelup.gitlab.com/learn/video/fy23-q4-quarterly-kickoff)
    1. Update the comms with the correct links
1. Fourth week of the last month of the current quarter - ensure time is scheduled for the chosen date for the CEO to send the comms
1. Second full week of the new quarter - ping the CEO to send the comms on the correct day. Attend the AMA which follows.

#### Schedule for remaining FY24 and first FY25 Quarterly Kickoffs

1. FY24-Q2: Review/Record 2023-05-08 and 2023-05-10. Share the video on 2023-05-15. AMA on 2023-05-17
1. FY24-Q3: Review/Record 2023-08-07 and 2023-08-09. Share the video on 2023-08-14. AMA on 2023-08-16
1. FY24-Q4: Review/Record 2023-11-06 and 2023-11-08. Share the video on 2023-11-13. AMA on 2023-11-15
1. FY25-Q1: Review/Record 2024-02-05 and 2024-02-07. Share the video on 2024-02-12. AMA on 2024-02-14

### How to prepare the Slides

**Note:** A member of the CoST is the DRI for this but we coordinate with Internal Comms, Legal, and the EBA team to get the slides prepared, recorded, shared, and AMA scheduled.
{: .note}

- Start by copying the last slide deck. This will form the skeleton
- Mission/Vision/Strategy may need to be updated to note the differences
- Top Cross-Functional Initiatives may need to be updated and notes made on one's retired and added
- OKRs may need to be updated for the quarter and scores/notes added
- Work with Internal Comms schedule the recordings and prepare the announcement to `#company-fyi-private` as well as an addition to the `While You Were Iterating` newsletter
- Work with Legal to ensure the deck and recordings are SAFE
- Work with `#eba-team` to get the AMA Scheduled and populate the AMA agenda

Be sure that slides are prepared with enough notice for the CEO to record a video and for it to be shared at least 24 hours in advance of the AMA.
